# sendmail

This is a small app that would allow users to send email.

The app uses 2 email service providers: [SendGrid](https://sendgrid.com) and [MailGun](https://www.mailgun.com). You'd need to create account and get an API Key to be able to run this app.

[Send Grid - Simple Send API Documentation](https://sendgrid.com/docs/API_Reference/Web_API_v3/index.html)
[Mail Gun - Simple Send API Documentation](https://documentation.mailgun.com/en/latest/api-sending.html)

## Limitation

For using Mail Gun service provider, you need to have a domain. If you don't have one, you can use the sandbox domain provided by Mail Gun, but you'll only be able to send emails to authorized recipients. https://help.mailgun.com/hc/en-us/articles/217531258

## Requirements
 - Node (>= 10)

## Development

#### Setup

Clone this repository

```bash
git clone git@bitbucket.org:stefanymarie/sendmail.git
cd sendmail
```

Install dependencies for both server and client

```
npm install
npm install --prefix client --cwd client
```

Copy a sample `.env` and provide the necessary values. Then run the server and client

```
cp server/env.sample server/.env
npm run dev
```

This will open http://localhost:3000 in your browser. To change the port for the client, create a `.env` file inside the `client` directory and set `PORT`.

The server is running in http://localhost:5000. If you change the port, be sure to change `client/package.json` "proxy" accordingly.

## Testing

To run test

```
npm run server-test
```

## TODO

- write tests for handling various responses from external API
- write tests for request body validations
