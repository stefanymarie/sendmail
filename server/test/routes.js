const supertest = require('supertest');
const chai = require('chai');
const nock = require('nock');
const app = require('../app');
const config = require('../config');

const request = supertest(app);
const expect = chai.expect;

describe('POST /mail/send', () => {
  const mail = {
    sender: 'someone@testing.com',
    recipients: ['someone@testing.com'],
    subject: 'TEST!',
    content: 'This is a test!'
  };

  afterEach(() => {
    nock.cleanAll();
  });

  it('fails on empty request body', done => {
    request
      .post('/mail/send')
      .send()
      .expect(422)
      .end((err, res) => {
        expect(res.body).to.have.property('errors');
        expect(res.body.errors).to.be.an('array');
        expect(res.body.errors).to.have.lengthOf(4);
        done();
      });
  });

  it('sends email to sendgrid', done => {
    nock(config.sendgrid.apiBaseURL)
      .post('/mail/send')
      .reply(202);

    request
      .post('/mail/send')
      .send(mail)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.be.empty;
        done();
      });
  });

  it('sends email to mailgun as failover', done => {
    nock(config.sendgrid.apiBaseURL)
      .post('/mail/send')
      .reply(401);
    nock(config.mailgun.apiBaseURL)
      .post(/messages$/)
      .reply(200);
    request
      .post('/mail/send')
      .send(mail)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.be.empty;
        done();
      });
  });

  it('sends error if both providers failed', done => {
    nock(config.sendgrid.apiBaseURL)
      .post('/mail/send')
      .reply(401);
    nock(config.mailgun.apiBaseURL)
      .post(/messages$/)
      .reply(400);
    request
      .post('/mail/send')
      .send(mail)
      .expect(500)
      .end((err, res) => {
        expect(res.body).to.have.property('message');
        expect(res.body.message).to.equal(
          'The server encountered an internal error.'
        );
        done();
      });
  });
});
