module.exports = {
  server: {
    port: process.env.PORT || 5000
  },
  sendgrid: {
    apiKey: process.env.SEND_GRID_API_KEY || 'secret',
    apiBaseURL: 'https://api.sendgrid.com/v3'
  },
  mailgun: {
    apiKey: process.env.MAIL_GUN_API_KEY || 'secret',
    apiBaseURL: 'https://api.mailgun.net/v3',
    defaultDomain: process.env.MAIL_GUN_DEFAULT_DOMAIN
  }
};
