require('dotenv').load();

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./mailer')(app);

if (process.env.NODE_ENV === 'production') {
  const clientBuildDir = path.join(__dirname, '../client/build');
  app.use(express.static(clientBuildDir));
  app.get('*', function(req, res) {
    res.sendFile(path.join(clientBuildDir, 'index.html'));
  });
}

module.exports = app;
