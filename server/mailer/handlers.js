const sendgrid = require('./email-providers/sendgrid');
const mailgun = require('./email-providers/mailgun');
const { validationResult } = require('express-validator/check');

const handleSendMailRequest = (request, response) => {
  const errors = validationResult(request);
  if (!errors.isEmpty()) {
    return response
      .status(422)
      .json({ errors: errors.array({ onlyFirstError: true }) });
  }

  sendgrid
    .sendMail(request)
    .then(() => response.status(202).json())
    .catch(error => {
      mailgun
        .sendMail(request)
        .then(() => response.status(202).json())
        .catch(error => {
          response
            .status(500)
            .json({ message: 'The server encountered an internal error.' });
        });
    });
};

exports.handleSendMailRequest = handleSendMailRequest;
