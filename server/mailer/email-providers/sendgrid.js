const axios = require('axios');
const config = require('../../config');

const buildEmailObjects = value => {
  let emails = value == null ? [] : Array.isArray(value) ? value : [value];
  return emails.map(email => ({ email }));
};

const transformSendMailRequest = data => {
  let personalization = {};
  const recipients = buildEmailObjects(data.recipients);
  if (recipients.length > 0) {
    personalization['to'] = recipients;
  }
  const cc_recipients = buildEmailObjects(data.cc_recipients);
  if (cc_recipients.length > 0) {
    personalization['to'] = cc_recipients;
  }
  const bcc_recipients = buildEmailObjects(data.bcc_recipients);
  if (bcc_recipients.length > 0) {
    personalization['to'] = bcc_recipients;
  }

  const newData = {
    personalizations: [personalization],
    from: { email: data.sender },
    subject: data.subject,
    content: [{ type: 'text/plain', value: data.content }]
  };
  return JSON.stringify(newData);
};

const sendMail = request => {
  const data = request.body;
  const { apiBaseURL, apiKey } = config.sendgrid;
  const options = {
    url: '/mail/send',
    method: 'POST',
    baseURL: apiBaseURL,
    transformRequest: [transformSendMailRequest],
    headers: {
      Authorization: `Bearer ${apiKey}`,
      'CONTENT-TYPE': 'application/json'
    },
    data: data,
    responseType: 'json',
    validateStatus: status => status === 202
  };

  return axios(options).catch(error => {
    if (error.response) {
      console.error(
        'Send Grid responded with an error',
        error.response.status,
        JSON.stringify(error.response.data)
      );
      throw error;
    }
    console.error('Failed to complete request to Send Grid', error.message);
    throw error;
  });
};

exports.sendMail = sendMail;
