const axios = require('axios');
const qs = require('qs');
const config = require('../../config');

const buildSendMailData = data => ({
  from: data.sender,
  to: data.recipients,
  cc: data.cc_recipients,
  bcc: data.bcc_recipients,
  subject: data.subject,
  text: data.content
});

const sendMail = request => {
  const data = request.body;
  const { apiBaseURL, apiKey, defaultDomain } = config.mailgun;

  const options = {
    url: `${defaultDomain}/messages`,
    method: 'POST',
    baseURL: apiBaseURL,
    headers: {
      'content-type': 'application/x-www-form-urlencoded'
    },
    data: qs.stringify(buildSendMailData(data)),
    auth: { username: 'api', password: apiKey },
    responseType: 'json',
    validateStatus: status => status === 200
  };
  console.log(options.data);

  return axios(options).catch(error => {
    if (error.response) {
      console.error(
        'Mail Gun responded with an error',
        error.response.status,
        JSON.stringify(error.response.data)
      );
      throw error;
    }
    console.error('Failed to complete request to Mail Gun', error.message);
    throw error;
  });
};

exports.sendMail = sendMail;
