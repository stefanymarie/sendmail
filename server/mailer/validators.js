var { check, checkSchema } = require('express-validator/check');

var arraySchema = {
  isArray: true,
  errorMessage: 'must be an array'
};

var emailSchema = {
  trim: true,
  isEmail: true,
  normalizeEmail: true,
  errorMessage: 'must be a valid email address',
};

var withContentSchema = {
  trim: true,
  isLength: {
    errorMessage: 'must not be empty',
    options: { min: 1 }
  }
};

var validators = [
  check('sender').exists().withMessage('is required'),
  check('recipients').exists().withMessage('is required'),
  check('subject').exists().withMessage('is required'),
  check('content').exists().withMessage('is required'),
  ...checkSchema({
    sender: emailSchema,
    recipients: { ...arraySchema, ...withContentSchema },
    'recipients.*': emailSchema,
    cc_recipients: { ...arraySchema, optional: true },
    'cc_recipients.*': emailSchema,
    bcc_recipients: { ...arraySchema, optional: true },
    'cc_recipients.*': emailSchema,
    subject: withContentSchema,
    content: withContentSchema
  })
];

module.exports = validators;
