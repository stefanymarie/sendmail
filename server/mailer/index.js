const validators = require('./validators');
const handleSendMailRequest = require('./handlers').handleSendMailRequest;

module.exports = (app) => {
  app.post('/mail/send', validators, handleSendMailRequest);
}
