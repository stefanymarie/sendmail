import React, { Component } from 'react';
import { Grid, Header, Icon } from 'semantic-ui-react';
import { SemanticToastContainer } from 'react-semantic-toasts';
import MailForm from './form/MailForm';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Grid container centered>
          <Grid.Row>
            <Header as='h2' icon>
              <Icon name='mail' />
              Send Email
            </Header>
          </Grid.Row>
          <Grid.Column width={12}>
            <MailForm />
          </Grid.Column>
        </Grid>
        <SemanticToastContainer position="top-right" />
      </React.Fragment>
    );
  }
}

export default App;
