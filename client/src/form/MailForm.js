import React, { Component } from 'react';
import { Form, List, Popup } from 'semantic-ui-react';
import { toast } from 'react-semantic-toasts';
import InputField from './InputField';
import EmailsField from './EmailsField';

const defaultValues = {
  sender: '',
  recipients: [],
  cc_recipients: [],
  bcc_recipients: [],
  subject: '',
  content: ''
};

class MailForm extends Component {
  state = {
    values: defaultValues,
    fieldShown: {
      cc: false,
      bcc: false
    },
    submitting: false,
    errors: {}
  };

  submitForm = () => {
    const options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(this.state.values)
    };
    return fetch('/mail/send', options)
      .then(response => {
        if (response.ok) {
          this.setState({ values: defaultValues, submitting: false }, () => {
            toast({
              type: 'success',
              title: 'Your message has been sent!',
              icon: 'check'
            });
          });
        } else {
          response.json().then(body => {
            const errors = body.errors || [];
            if (errors.length > 0) {
              this.setState({
                errors: errors.reduce((obj, error) => {
                  return { ...obj, [error.param]: error.msg };
                }, {}),
                submitting: false
              });
              return;
            }
            if (body.message) {
              this.setState({ submitting: false }, () => {
                toast({
                  type: 'error',
                  title: 'Error',
                  description: `${body.message}`,
                  icon: 'remove'
                });
              });
            }
          });
        }
      })
      .catch(err => {
        console.log('Error:', err);
      });
  };

  handleInputChange = (e, { name, value }) => {
    this.setState({
      values: {
        ...this.state.values,
        [name]: value
      },
      errors: {
        ...this.state.errors,
        [name]: null
      }
    });
  };

  handleSubmit = async e => {
    e.preventDefault();
    this.setState({ submitting: true }, this.submitForm);
  };

  toggleFieldVisibility = (e, { field }) => {
    this.setState({
      fieldShown: {
        ...this.state.fieldShown,
        [field]: !this.state.fieldShown[field]
      }
    });
  };

  render() {
    const {
      values: {
        sender,
        recipients,
        cc_recipients,
        bcc_recipients,
        subject,
        content
      },
      fieldShown,
      submitting,
      errors
    } = this.state;

    return (
      <div>
        <Form onSubmit={this.handleSubmit} autoComplete="off">
          <InputField
            component={Form.Input}
            label="From"
            placeholder="Your email"
            name="sender"
            value={sender}
            type="email"
            onChange={this.handleInputChange}
            error={errors.sender}
          />
          <InputField
            component={EmailsField}
            label="To"
            placeholder="Recipients"
            name="recipients"
            value={recipients}
            onChange={this.handleInputChange}
            error={errors.recipients}
          />
          {fieldShown.cc && (
            <InputField
              component={EmailsField}
              label="Cc"
              placeholder="Cc Recipients"
              name="cc_recipients"
              value={cc_recipients}
              onChange={this.handleInputChange}
              error={errors.cc_recipients}
            />
          )}
          {fieldShown.bcc && (
            <InputField
              component={EmailsField}
              label="Bcc"
              placeholder="Bcc Recipients"
              name="bcc_recipients"
              value={bcc_recipients}
              onChange={this.handleInputChange}
              error={errors.bcc_recipients}
            />
          )}
          {(!fieldShown.cc || !fieldShown.bcc) && (
            <List
              horizontal
              link
              size="tiny"
              floated="right"
              style={{ marginTop: -7 }}
            >
              {!fieldShown.cc && (
                <Popup
                  inverted
                  size="mini"
                  basic
                  trigger={
                    <List.Item
                      href="#"
                      field="cc"
                      onClick={this.toggleFieldVisibility}
                      children="CC"
                    />
                  }
                  content="Add Cc Recipients"
                  position="bottom right"
                />
              )}
              {!fieldShown.bcc && (
                <Popup
                  inverted
                  size="mini"
                  basic
                  trigger={
                    <List.Item
                      href="#"
                      field="bcc"
                      onClick={this.toggleFieldVisibility}
                      children="BCC"
                    />
                  }
                  content="Add Bcc Recipients"
                  position="bottom right"
                />
              )}
            </List>
          )}
          <InputField
            component={Form.Input}
            label="Subject"
            placeholder="Subject"
            name="subject"
            value={subject}
            onChange={this.handleInputChange}
            error={errors.subject}
          />
          <InputField
            component={Form.TextArea}
            placeholder="Your message"
            name="content"
            value={content}
            rows={5}
            onChange={this.handleInputChange}
            error={errors.content}
          />
          <Form.Button
            content={submitting ? 'Sending...' : 'Send'}
            disabled={submitting}
            primary
          />
        </Form>
      </div>
    );
  }
}

export default MailForm;
