import React, { Component } from 'react';
import { Popup } from 'semantic-ui-react';

class InputField extends Component {
  state = {
    showError: false
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error !== this.props.error && !this.hasError(nextProps)) {
      this.handleClose();
    }
  }

  handleOpen = () => this.setState({ showError: this.hasError(this.props) });

  handleClose = () => this.setState({ showError: false });

  hasError = props => {
    return (
      typeof props.error !== 'undefined' &&
      props.error &&
      props.error.length > 0
    );
  };

  render() {
    const { error, component, ...inputProps } = this.props;
    const hasError = this.hasError(this.props);

    const Field = component;
    return (
      <Popup
        trigger={<Field {...inputProps} error={hasError} />}
        on="focus"
        content={error}
        open={this.state.showError}
        onClose={this.handleClose}
        onOpen={this.handleOpen}
        size="tiny"
        wide
      />
    );
  }
}

export default InputField;
