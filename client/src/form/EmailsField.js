import React, { Component } from 'react';
import { Form, Dropdown } from 'semantic-ui-react';

const validPattern = new RegExp(/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/);

class EmailsField extends Component {
  state = {
    options: [],
    searchQuery: ''
  };

  dropdown = React.createRef();

  componentWillReceiveProps(nextProps) {
    const newValue = nextProps.value;
    const currentOptions = this.state.options.map(o => o.value);

    if (JSON.stringify(newValue) !== JSON.stringify(currentOptions)) {
      this.setState({
        options: newValue.map(value => ({ value, text: value }))
      });
    }
  }

  handleChange = (e, { value }) => {
    const validValue = value.filter(v => this.isEmail(v));
    const inputValue = { value: validValue, name: this.props.name };
    this.setState({ searchQuery: '' });
    this.props.onChange && this.props.onChange(e, inputValue);
  };

  handleSearchChange = (e, { searchQuery }) => {
    const emails = this.getEmails([searchQuery]);
    if (emails.length > 1) {
      this.handleChange(e, { value: [...this.props.value, ...emails] });
    } else {
      this.setState({ searchQuery });
    }
    if (this.props.error) {
      this.props.onChange(e, {
        name: this.props.name,
        value: this.props.value
      });
    }
  };

  handleBlur = e => {
    const { searchQuery } = this.state;
    const emails = this.getEmails([searchQuery]);
    if (emails.find(e => this.isEmail(e))) {
      this.handleChange(e, { value: [...this.props.value, ...emails] });
      this.dropdown.current.open();
    } else {
      this.setState({ searchQuery: '' });
    }
  };

  renderLabel = label => ({
    content: label.text,
    style: { fontSize: 13, fontWeight: 'normal' }
  });

  isEmail = value => value.match(validPattern);

  getEmails = value => {
    return value.reduce((emails, str) => {
      const values = str.split(/[\s]+/).map(v => v.trim());
      return [...emails, ...values];
    }, []);
  };

  render() {
    const { label, ...dropdownProps } = this.props;
    return (
      <Form.Field error={dropdownProps.error}>
        <label>{label}</label>
        <Dropdown
          {...dropdownProps}
          className="no-add-menu"
          ref={this.dropdown}
          options={this.state.options}
          searchQuery={this.state.searchQuery}
          search
          selection
          fluid
          multiple
          allowAdditions
          noResultsMessage={null}
          icon={null}
          renderLabel={this.renderLabel}
          onChange={this.handleChange}
          onSearchChange={this.handleSearchChange}
          onBlur={this.handleBlur}
        />
      </Form.Field>
    );
  }
}

export default EmailsField;
